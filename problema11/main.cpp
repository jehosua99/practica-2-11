#include <iostream>

using namespace std;
char impresiondeasientos();  //Funcion que imprime la silleteria
int caracteranumero(char);  //Cambiara la letra ingresada por un numero que sera entendido por el sistema

int main()
{
    cout << impresiondeasientos() << "\n";
    return 0;
}

char impresiondeasientos()
{
    char opcion, accion = 'y';
    char asientos [15][20];
    for (int i = 0; i <= 14; i ++)  //Ciclo que recorre el numero de filas y crea la matriz
    {
        for (int j = 0; j <= 19; j ++)  //Ciclo que recorre el numero de asientos
            asientos[i][j] = '-';  //Busco la fila y por cada asiento agrego un "-" cuando termina sigue a la siguiente fila
    }
    while (accion == 'y')  //Ciclo que siempre y cuando se necesiten hacer mas modificaciones se repetira
    {
        cout << "¿Desea realizar una reserva (r) o una cancelacion (c)?\n";
        cin >> opcion;
        if (opcion != 'r' || opcion != 'c')
            cout << "No ha ingresado una opcion valida\n";
        if (opcion == 'r')  //opcion para generar reservas
        {
            int puesto, fila;
            char letrafila;
            cout << "Ingrese la letra de la fila en la cual realizara la reserva (A - O):\n";
            cin >> letrafila;  //Pido la fila que sera modificada
            cout << "Ingrese el numero de asiento que reservara (1 - 20):\n";
            cin >> puesto; //Pido el numero de asiento que sera reservado
            fila = caracteranumero(letrafila);
            asientos[fila][puesto-1] = '+';
            for (int i = 0; i <= 14; i ++)  //Recorro de nuevo las filas
            {
                for (int j = 0; j <= 19; j ++)  //Uuna vez impresa la linea divisoria horizontal recorro los asientos

                    cout << "  " << asientos[i][j]; //Agrego la linea divisoria vertical e imprimo el valor de cada asiento dependiendo si esta reservado o no

                cout << "\n"; //Al terminar el recorrido por la fila saltara de linea para volver a hacer el ciclo
            }
        }
        if (opcion == 'c')  //opcion para generar cancelaciones
        {
            int puesto, fila;
            char letrafila;
            cout << "Ingrese la letra de la fila en la cual realizara la cancelacion (A - O):\n";
            cin >> letrafila;  //Pido la fila que sera modificada
            cout << "Ingrese el numero de asiento que cancelara (1 - 20):\n";
            cin >> puesto; //Pido el numero de asiento que sera reservado
            fila = caracteranumero(letrafila);
            asientos[fila][puesto-1] = '-';
            for (int i = 0; i <= 14; i ++)  //Recorro de nuevo las filas
            {
                for (int j = 0; j <= 19; j ++)  //Uuna vez impresa la linea divisoria horizontal recorro los asientos

                    cout << "  " << asientos[i][j]; //Agrego la linea divisoria vertical e imprimo el valor de cada asiento dependiendo si esta reservado o no

                cout << "\n"; //Al terminar el recorrido por la fila saltara de linea para volver a hacer el ciclo
            }
        }
    cout << "¿Desea realizar una nueva accion? (y/n)\n";
    cin >> accion;
    }
    return 0;
}

int caracteranumero(char a)
{
    int fila = 0;
    if (a == 'A' || a == 'a')
        fila = 0;
    else if (a == 'B' || a == 'b')
        fila = 1;
    else if (a == 'C' || a == 'c')
        fila = 2;
    else if (a == 'D' || a == 'd')
        fila = 3;
    else if (a == 'E' || a == 'e')
        fila = 4;
    else if (a == 'F' || a == 'f')
        fila = 5;
    else if (a == 'G' || a == 'g')
        fila = 6;
    else if (a == 'H' || a == 'h')
        fila = 7;
    else if (a == 'I' || a == 'i')
        fila = 8;
    else if (a == 'J' || a == 'j')
        fila = 9;
    else if (a == 'K' || a == 'k')
        fila = 10;
    else if (a == 'L' || a == 'l')
        fila = 11;
    else if (a == 'M' || a == 'm')
        fila = 12;
    else if (a == 'N' || a == 'n')
        fila = 13;
    else if (a == 'O' || a == 'o')
        fila = 14;
    return fila;
}
